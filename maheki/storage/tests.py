# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit test for storage
"""
from datetime import datetime
from django.test import TestCase
from maheki.graph.models import Bench
from maheki.graph.models import Campaign
from maheki.graph.models import Client
from maheki.graph.models import Run
from maheki.graph.models import Host
from maheki.storage.models import Pom


class StorageTest(TestCase):
    def setUp(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.client = Client.objects.create(name='sample foo client')

        self.host = Host.objects.create(name='sample foo',
                                        client=self.client)

        self.campaign = Campaign.objects.create(name='sample foo',
                                                client=self.client,
                                                start=datetime.now(),
                                                description='lorem ipsum')

        self.bench = Bench.objects.create(campaign=self.campaign,
                                          name='sample foo')

        self.run = Run.objects.create(bench=self.bench,
                                      start=datetime.now(),
                                      stop=datetime.now())

    def test_create_pom(self):
        idp = Pom.objects.create(run=self.run,
                                 name='tr_home',
                                 datetms=4.4,
                                 value=4)

        self.assertTrue(idp > 0)
