# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
The django views
"""
from django.shortcuts import render
from django.views.generic import DetailView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from maheki.graph.models import Client
from maheki.graph.models import Campaign
from maheki.graph.models import Run
from tastypie.models import ApiKey
from django.shortcuts import redirect


@login_required
def profile(request):
    """The profile wiew
    """
    template_name = 'profile.html'

    runs = Run.objects.filter(bench__campaign__client__users__in=[request.user.id]).order_by('-start')[:5]
    clients = Client.objects.filter(users__pk=request.user.id)
    campaigns = Campaign.objects.filter(client__users__in=[request.user.id]).order_by("-start")[:5]

    return render(request,
                  template_name,
                  {'campaigns': campaigns,
                   'clients': clients,
                   'runs': runs})


@login_required
def profile_api(request):
    """The api key view
    """
    template_name = 'profile_api.html'

    key = ApiKey.objects.get(user=request.user)

    return render(request,
                  template_name,
                  {'apikey': key})


@login_required
def profile_api_renew(request):
    """Generate a new api key
    """
    api_key = ApiKey.objects.get_or_create(user=request.user)[0]

    api_key.key = api_key.generate_key()
    api_key.save()

    return redirect('/accounts/profile/apikey')


class ClientDetailView(DetailView):

    model = Client

    def get_object(self, **kwargs):
        obj = super(ClientDetailView, self).get_object()
        if self.request.user not in obj.users.all():
            raise PermissionDenied
        else:
            return obj

    def get_context_data(self, **kwargs):
        context = super(ClientDetailView, self).get_context_data(**kwargs)
        context['campaigns'] = Campaign.objects.filter(client=self.object)
        context['users'] = User.objects.filter(client__in=[self.object])

        return context
