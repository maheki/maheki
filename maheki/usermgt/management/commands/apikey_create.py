#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Create an API key
"""
import sys
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from tastypie.models import ApiKey


class Command(BaseCommand):
    args = 'username'
    help = 'Print all apikeys on stdout'

    def handle(self, *args, **options):
        """
        Create a key for a user
        """
        try:
            username = args[0]
        except IndexError:
            print "you must indicate a key on args"
            sys.exit(1)

        try:
            user = User.objects.get(username=username)
        except ObjectDoesNotExist:
            print "Failed, user {} does not exists".format(username)
            sys.exit(1)

        create = False


        (api_key, create) = ApiKey.objects.get_or_create(user=user)
        if not create:
            print "Key {} exists for {}".format(api_key.key, user.username)
        else:
            api_key.key = api_key.generate_key()
            api_key.save()
            print "Key {} created for {}".format(api_key.key, user)
