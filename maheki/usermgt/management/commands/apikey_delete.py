#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Delete an API key
"""
import sys
from django.core.management.base import BaseCommand
from tastypie.models import ApiKey


class Command(BaseCommand):
    args = 'key to be deleted'
    help = 'Print all apikeys on stdout'

    def handle(self, *args, **options):
        """
        Delete the api key passed as argument
        """
        try:
            self.delete_key(args[0])
        except IndexError:
            sys.stdout.write("you must indicate a key on args\n")
            sys.exit(1)

    def delete_key(self, key):
        """Delete the key
        """
        try:
            apikey = ApiKey.objects.get(key=key)
            apikey.delete()
            sys.stdout.write("Key {} deleted\n".format(apikey.key))
        except:
            sys.stdout.write("Key {} does not exists\n".format(key))
