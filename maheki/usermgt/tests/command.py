# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for Maheki usermgt apps
"""
from StringIO import StringIO
from django.core.management import call_command
from django.test import TestCase
from django.contrib.auth.models import User
from tastypie.models import ApiKey


class CommandTests(TestCase):  # pylint: disable-msg=R0904
    """
    Test the management commands in usermgt
    """
    def setUp(self):
        """
        Init
        """
        User.objects.all().delete()
        ApiKey.objects.all().delete()

    def test_user_list(self):
        """
        List all users
        """
        user = User.objects.create(username='command_user_list')

        attend = 'command_user_list\n'.format(user.username)

        content = StringIO()
        call_command('user_list', stdout=content)
        content.seek(0)

        self.assertEqual(content.read(), attend)

    def test_apikey_list(self):
        """
        List all api keys
        """
        user = User.objects.create(username='command_apikey_list')
        api_key = ApiKey.objects.get_or_create(user=user)[0]
        api_key.key = api_key.generate_key()
        api_key.save()

        attend = '{} command_apikey_list\n'.format(api_key.key)

        content = StringIO()
        call_command('apikey_list', stdout=content)
        content.seek(0)

        self.assertEqual(content.read(), attend)

    def test_apikey_delete(self):
        """
        Delete an existing api key
        """
        user = User.objects.create(username='command_apikey_list')
        result = ApiKey.objects.get_or_create(user=user)
        api_key = result[0]
        api_key.key = api_key.generate_key()
        api_key.save()

        before = ApiKey.objects.all().count()

        content = StringIO()
        call_command('apikey_delete', api_key.key, stdout=content)
        content.seek(0)

        after = ApiKey.objects.all().count()

        self.assertEqual(before, 1)
        self.assertEqual(after, 0)

    def test_apikey_delete_omitted(self):
        """
        Delete an api key
        Key args is ommitted
        """
        user = User.objects.create(username='command_apikey_list')
        api_key = ApiKey.objects.get_or_create(user=user)[0]
        api_key.key = api_key.generate_key()
        api_key.save()
        key = api_key.key

        before = ApiKey.objects.all().count()

        content = StringIO()
        with self.assertRaises(SystemExit) as cm:
            call_command('apikey_delete', stdout=content)
        content.seek(0)

        after = ApiKey.objects.all().count()

        self.assertEqual(before, 1)
        self.assertEqual(after, 1)
        self.assertEqual(cm.exception.code, 1)

    def test_apikey_delete_notexists(self):
        """
        Delete an api key
        Key does not exists
        """
        user = User.objects.create(username='command_apikey_list')
        result = ApiKey.objects.get_or_create(user=user)
        api_key = result[0]
        api_key.key = api_key.generate_key()
        api_key.save()

        before = ApiKey.objects.all().count()

        key = 'foobar42'

        content = StringIO()
        call_command('apikey_delete', key, stdout=content)
        content.seek(0)

        after = ApiKey.objects.all().count()

        self.assertEqual(before, 1)
        self.assertEqual(after, 1)

    def test_apikey_create(self):
        """
        Create an api key
        """
        before = ApiKey.objects.all().count()
        user = User.objects.create(username='command_apikey_create')

        content = StringIO()
        call_command('apikey_create', user.username, stdout=content)
        content.seek(0)

        after = ApiKey.objects.all().count()

        self.assertEqual(before, 0)
        self.assertEqual(after, 1)

    def test_apikey_create_missing_user(self):
        """
        Create an api key
        Assert : system exit code = 1
        """
        before = ApiKey.objects.all().count()
        user = User.objects.create(username='command_apikey_create')
        ApiKey.objects.all().delete()

        content = StringIO()
        with self.assertRaises(SystemExit) as cm:
            call_command('apikey_create', stdout=content)
        content.seek(0)

        after = ApiKey.objects.all().count()

        self.assertEqual(before, 0)
        self.assertEqual(after, 0)
        self.assertEqual(cm.exception.code, 1)

    def test_apikey_create_user_not_exists(self):
        """
        Create an api key
        Assert : system exit code = 1
        """
        before = ApiKey.objects.all().count()
        user = User.objects.create(username='command_apikey_create')
        ApiKey.objects.all().delete()

        content = StringIO()
        with self.assertRaises(SystemExit) as cm:
            call_command('apikey_create', 'user_not_exists', stdout=content)
        content.seek(0)

        after = ApiKey.objects.all().count()

        self.assertEqual(before, 0)
        self.assertEqual(after, 0)
        self.assertEqual(cm.exception.code, 1)

    def test_apikey_create_key_exists(self):
        """
        Create an api key
        - the key is still exists
        """
        before = ApiKey.objects.all().count()
        user = User.objects.create(username='command_apikey_create_ke')
        api_key = ApiKey.objects.get_or_create(user=user)[0]
        api_key.key = api_key.generate_key()
        api_key.save()

        content = StringIO()
        call_command('apikey_create', user.username, stdout=content)
        content.seek(0)

        after = ApiKey.objects.all().count()

        self.assertEqual(before, 0)
        self.assertEqual(after, 1)
