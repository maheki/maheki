# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for views in Maheki usermgt
"""
from datetime import datetime
from json import loads as json_loads
from django.test import TestCase
from django.test import Client as TestClient
from django.test import RequestFactory
from django.contrib.auth.models import User
from tastypie.models import ApiKey
from maheki.graph.models import Campaign
from maheki.graph.models import Client
from maheki.graph.models import Bench
from maheki.graph.models import Run
from maheki.graph.models import Measure
from maheki.usermgt import views


class ViewsTests(TestCase):  # pylint: disable-msg=R0904
    """
    Test all the views (or try to do)
    """
    def setUp(self):
        """
        Init
        """
        self.client = Client.objects.create(name='sample foo client')

        self.campaign = Campaign.objects.create(name='The default Campaign VT',
                                                client=self.client,
                                                start=datetime.now(),
                                                description='lorem ipsum')

        self.bench = Bench.objects.create(campaign=self.campaign,
                                          name='The default bench')

        self.run = Run.objects.create(bench=self.bench,
                                      code="2013",
                                      start=datetime.now(),
                                      stop=datetime.now())

    def test_profile(self):
        """The profile view
        - profile view list client's campaign
        """
        # Setup name.
        user = User.objects.create_user('views_profile',
                                        'email@fdn.com',
                                        'foo42')
        client = Client.objects.create(name='client 0d41')
        client.users.add(user.id)

        campaign = Campaign.objects.create(name='0d41382211e32023e2898815372066',
                                           client=client,
                                           start=datetime.now(),
                                           description='lorem ipsum')

        # Setup request and view.
        client = TestClient()
        client.login(username='views_profile', password='foo42')
        response = client.get('/')
        # Check.
        self.assertContains(response, 'client 0d41', status_code=200)
        self.assertContains(response, '0d41382211e32023e2898815372066', status_code=200)

    def test_profile_api(self):
        """The api key profile view

        """
        # Setup name.
        user = User.objects.create_user('views_profile_api',
                                        'email@fdn.com',
                                        'foo42')
        api_key = ApiKey.objects.get_or_create(user=user)[0]
        api_key.key = api_key.generate_key()
        api_key.save()


        # Setup request and view.
        client = TestClient()
        client.login(username='views_profile_api', password='foo42')
        response = client.get('/accounts/profile/apikey')

        # Check.
        self.assertContains(response, 'views_profile_api', status_code=200)
        self.assertContains(response, api_key.key, status_code=200)

    def test_profile_renew(self):
        """Renew the api key
        - Renew the api key
        """
        # Setup name.
        user = User.objects.create_user('views_profile_api',
                                        'email@fdn.com',
                                        'foo42')
        api_key = ApiKey.objects.get_or_create(user=user)[0]
        api_key.key = api_key.generate_key()
        api_key.save()


        # Setup request and view.
        client = TestClient()
        client.login(username='views_profile_api', password='foo42')
        response = client.get('/accounts/profile/apikey/renew')

        new_key = ApiKey.objects.get_or_create(user=user)[0]

        # Check.
        self.assertEqual(response.status_code, 302)
        self.assertNotEqual(api_key.key, new_key.key)
