from django.conf.urls import patterns, include, url
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.contrib import admin
from maheki.graph.models import Campaign
from maheki.graph.models import Bench
from maheki.graph.models import Host
from maheki.graph.models import Run
from maheki.graph.models import Software
from maheki.graph.models import GraphDefinition
from maheki.graph.views import CampaignDetailView
from maheki.graph.views import CampaignListView
from maheki.graph.views import NfrListView
from maheki.graph.views import GraphDefinitionListView
from maheki.graph.views import HostDetailView
from maheki.graph.views import HostListView
from maheki.graph.views import BenchDetailView
from maheki.graph.views import BenchListView
from maheki.graph.views import RunDetailView
from maheki.graph.views import NfrDetailView
from maheki.graph.views import RunReportDetailView
from maheki.graph.views import TransactionDetailView
from tastypie.api import Api
from maheki.graph.api import ClientResource
from maheki.graph.api import BenchResource
from maheki.graph.api import RunResource
from maheki.graph.api import MeasureResource
from maheki.graph.api import SoftwareResource
from maheki.graph.api import UrlResource
from maheki.graph.api import NfrResource
from maheki.graph.api import RequirementResource
from maheki.storage.api import PomResource
from maheki.usermgt.views import ClientDetailView

# Implement API version 1                                                                                
v1_api = Api(api_name='v1')
# Register Shows                                                                                         
v1_api.register(BenchResource())
v1_api.register(ClientResource())
v1_api.register(MeasureResource())
v1_api.register(NfrResource())
v1_api.register(RunResource())
v1_api.register(SoftwareResource())
v1_api.register(UrlResource())
v1_api.register(PomResource())
v1_api.register(RequirementResource())

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^about/', TemplateView.as_view(template_name="about.html")),
    url(r'^lastrun$', 'maheki.graph.views.lastrun'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^accounts/profile/apikey/renew$', 'maheki.usermgt.views.profile_api_renew'),
    url(r'^accounts/profile/apikey$', 'maheki.usermgt.views.profile_api'),
    url(r'^accounts/profile/$', 'maheki.usermgt.views.profile'),
    url(r'^graph/(?P<pk>\d+)/(?P<host>\d+)$', 'maheki.graph.views.one_graph'),
    url(r'^$', 'maheki.usermgt.views.profile', name='home'),
    url(r'^campaigns$', login_required(CampaignListView.as_view())),
    url(r'^campaign/(?P<pk>\d+)$', login_required(CampaignDetailView.as_view())),
    url(r'^softwares$', ListView.as_view(model=Software,
                                         queryset=Software.objects.order_by("name"))),
    url(r'^definition/(?P<family>\w+)$', login_required(GraphDefinitionListView.as_view())),
    url(r'^definition$', login_required(ListView.as_view(model=GraphDefinition))),
    # 
    url(r'^hosts$', login_required(HostListView.as_view())),
    url(r'^host/(?P<pk>\d+)/(?P<did>\d+)$', login_required(HostDetailView.as_view())),
    url(r'^host/(?P<pk>\d+)$', login_required(HostDetailView.as_view())),
    # bench
    url(r'^benchs$', login_required(BenchListView.as_view())),
    url(r'^bench/(?P<bench>\d+)/metric/(?P<metric>\w+)/(?P<name>\w+)/json$', 'maheki.graph.views.measure_datas'),
    url(r'^bench/(?P<bench>\d+)/metric/(?P<metric>\w+)/json$', 'maheki.graph.views.measure_datas'),
    # transaction
    url(r'^bench/(?P<pk>\d+)/t/(?P<trans_name>\w+)/(?P<metric>\w+)$', login_required(TransactionDetailView.as_view())),
    url(r'^bench/(?P<pk>\d+)/t/(?P<trans_name>\w+)$', login_required(TransactionDetailView.as_view())),
    url(r'^bench/(?P<pk>\d+)$', login_required(BenchDetailView.as_view())),
    # run
    url(r'^run/(?P<pk>\d+)/report$', login_required(RunReportDetailView.as_view())),
    url(r'^run/(?P<pk>\d+)$', login_required(RunDetailView.as_view())),
    # nfr
    url(r'^nfr/(?P<pk>\d+)/(?P<name>\w+)$', login_required(NfrDetailView.as_view())),
    url(r'^nfr/(?P<pk>\d+)$', login_required(NfrDetailView.as_view())),
    url(r'^nfr$', login_required(NfrListView.as_view())),
    url(r'^api/', include(v1_api.urls)),
    #
    url(r'^client/(?P<pk>\d+)$', login_required(ClientDetailView.as_view())),
)
