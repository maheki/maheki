# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from maheki.graph.models import Bench
from maheki.graph.models import Run
from maheki.graph.models import Campaign
from maheki.graph.models import Client
from maheki.graph.models import Service
from maheki.graph.models import Software
from maheki.graph.models import Softcat
from maheki.graph.models import Host
from maheki.graph.models import Nfr
from maheki.graph.models import Requirement
from maheki.graph.models import Measure
from django.contrib import admin


class RunAdmin(admin.ModelAdmin):
    """
    Custom Admin for Run
    """
    list_display = ('code', 'bench', 'start', 'stop')
    list_filter = ('bench',)


class BenchAdmin(admin.ModelAdmin):
    """
    Custom Admin for Bench
    """
    list_display = ('name',)


class CampaignAdmin(admin.ModelAdmin):
    """
    Custom Admin for Campaign
    """
    list_display = ('name',)
    list_filter = ('client',)


class ClientAdmin(admin.ModelAdmin):
    """
    Custom Admin for Client
    """
    list_display = ('name',)
    ordering = ['name']


class ServiceAdmin(admin.ModelAdmin):
    """
    Custom Admin for Service
    """
    list_display = ('name', 'description')
    ordering = ['name']


class SoftwareAdmin(admin.ModelAdmin):
    """
    Custom Admin for Software
    """
    list_display = ('name', 'description')
    ordering = ['name']
    list_filter = ('category',)


class SoftcatAdmin(admin.ModelAdmin):
    """
    Custom Admin for Softcat
    """
    list_display = ('name', 'description')
    ordering = ['name']


class HostAdmin(admin.ModelAdmin):
    """
    Custom Admin for Host
    """
    list_display = ('name',)
    list_filter = ('client',)


class NfrAdmin(admin.ModelAdmin):
    """
    Custom Admin for Non functionnal requirement
    """
    list_display = ('name',)


class RequirementAdmin(admin.ModelAdmin):
    """
    Custom Admin for Non functionnal requirement
    """
    list_display = ('nfr', 'name', 'metric', 'warn', 'crit')
    ordering = ['nfr']


class MeasureAdmin(admin.ModelAdmin):
    """
    Custom Admin for Measure
    """
    list_display = ('run', 'name', 'metric','value')
    list_filter = ('run', )


admin.site.register(Campaign, CampaignAdmin)
admin.site.register(Run, RunAdmin)
admin.site.register(Bench, BenchAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(Software, SoftwareAdmin)
admin.site.register(Softcat, SoftcatAdmin)
admin.site.register(Host, HostAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Nfr, NfrAdmin)
admin.site.register(Requirement, RequirementAdmin)
admin.site.register(Measure, MeasureAdmin)
