# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
The django views
"""
import json
from time import mktime
from django.conf import settings
from django.shortcuts import render
from django.views.generic import DetailView
from django.views.generic import ListView
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.core.cache import cache
from django.utils import simplejson
from django.http import HttpResponse
from django.db import models
from maheki.graph.models import Bench
from maheki.graph.models import Campaign
from maheki.graph.models import DashItem
from maheki.graph.models import GraphDefinition
from maheki.graph.models import Host
from maheki.graph.models import HostOption
from maheki.graph.models import Run
from maheki.graph.models import Nfr
from maheki.graph.models import Requirement
from maheki.graph.models import Measure


class GraphDefinitionListView(ListView):
    model = GraphDefinition

    def get_queryset(self):
        return GraphDefinition.objects.all().order_by('family')


class CampaignListView(ListView):
    model = Campaign

    def get_queryset(self):
        return Campaign.objects.filter(client__users__in=[self.request.user]).order_by('-pk')


class NfrListView(ListView):
    model = Nfr

    def get_queryset(self):
        return Nfr.objects.filter(bench__campaign__client__users__in=[self.request.user])


class HostListView(ListView):
    model = Host

    def get_queryset(self):
        return Host.objects.filter(client__users__in=[self.request.user])


class BenchListView(ListView):
    model = Bench

    def get_queryset(self):
        return Bench.objects.filter(campaign__client__users__in=[self.request.user]).order_by("-pk")


class CampaignDetailView(DetailView):

    model = Campaign

    def get_object(self, **kwargs):
        obj = super(CampaignDetailView, self).get_object()
        if self.request.user not in obj.client.users.all():
            raise PermissionDenied
        else:
            return obj

    def get_context_data(self, **kwargs):
        context = super(CampaignDetailView, self).get_context_data(**kwargs)
        context['benchs'] = Bench.objects.filter(campaign=self.object)
        return context


class HostDetailView(DetailView):

    model = Host

    def get_object(self, **kwargs):
        obj = super(HostDetailView, self).get_object()
        if self.request.user not in obj.client.users.all():
            raise PermissionDenied
        else:
            return obj

    def get_context_data(self, **kwargs):
        context = super(HostDetailView, self).get_context_data(**kwargs)
        context['width'] = settings.GRAPH_WIDTH
        context['graphite_server'] = settings.GRAPHITE_SERVER
        context['period'] = "from=-2h"
        context['bgcolor'] = "white"
        context['fgcolor'] = "black"
        context['target'] = self.object.alphacode

        self.object.get_options()

        opts = HostOption.objects.filter(host=self.object, name='default_dashboard')
        pref_dash = None
        if len(opts) > 0:
            pref_dash = int(opts[0].value)

        if 'did' in self.kwargs.keys():
            dash_id = int(self.kwargs['did'])
        else:
            if pref_dash:
                dash_id = pref_dash
            else:
                dash_id = settings.DEFAULT_DASHBOARD

        #pks = DashItem.objects.filter(dash__pk=dash_id).values_list('graph', flat=True).order_by('order')
        #graphdefs = GraphDefinition.objects.filter(pk__in=pks)

        dits = DashItem.objects.filter(dash__pk=dash_id).order_by('order')
        graphdefs = []
        for dit in dits:
            graphdefs.append(GraphDefinition.objects.filter(pk=dit.graph.id)[0])

        graphs = []
        for graph in graphdefs:
            if graph.family == 'disk':
                for disk in self.object.disks():
                    graph.title = graph.title.replace("%%disk%%", disk)
                    graphs.append(graph)
            else:
                graphs.append(graph)

        context['graphs'] = graphs
        return context


class BenchDetailView(DetailView):

    model = Bench

    def get_context_data(self, **kwargs):
        context = super(BenchDetailView, self).get_context_data(**kwargs)
        context['runs'] = Run.objects.filter(bench=self.object).order_by('-start')
        return context


class TransactionDetailView(DetailView):

    model = Bench
    template_name = "graph/transaction_report.html"

    def get_context_data(self, **kwargs):
        context = super(TransactionDetailView, self).get_context_data(**kwargs)
        if 'metric' in self.kwargs:
            context['metric'] = self.kwargs['metric']
        else:
            context['metric'] = "max"

        context['runs'] = Run.objects.filter(bench=self.object).order_by('-start')
        context['measures'] = Measure.objects.filter(run__in=context['runs'],
                                                     name=self.kwargs['trans_name']).order_by('-run')
        context['trans_name'] = self.kwargs['trans_name']

        nfr = Nfr.objects.filter(bench=self.object)
        if nfr:
            requirements = Requirement.objects.filter(nfr=nfr,
                                                      name=self.kwargs['trans_name'],
                                                      metric=context['metric'])[0]
            context['requirement'] = requirements

            key = 'bench_{}_metrics_json'.format(self.object.id)
            cval = cache.get(key)
            if cval is None:
                cdatas = Requirement.objects.filter(nfr=nfr,
                                                    name=self.kwargs['trans_name']).values('metric')
                rdatas = []
                for req in cdatas:
                    rdatas.append({'metric', req['metric']})
                # cache for one hour
                cache.set(key, json.dumps(cdatas), 3600)
            else:
                datas = json.loads(cval)

        return context


class NfrDetailView(DetailView):

    model = Nfr

    def get_context_data(self, **kwargs):
        context = super(NfrDetailView, self).get_context_data(**kwargs)

        if 'name' in self.kwargs:
            context['requs'] = Requirement.objects.filter(nfr=self.object,
                                                          name=self.kwargs['name'])
        else:
            context['requs'] = Requirement.objects.filter(nfr=self.object).order_by('name')

        return context


class RunDetailView(DetailView):

    model = Run

    def graphdefs(self):
        """

        """
        dash_id = settings.DEFAULT_DASHBOARD
        dits = DashItem.objects.filter(dash__pk=dash_id).order_by('order')
        graphs = []
        for dit in dits:
            graphs.append(GraphDefinition.objects.get(pk=dit.graph.id))
        return graphs

    def get_context_data(self, **kwargs):
        context = super(RunDetailView, self).get_context_data(**kwargs)
        context['width'] = settings.GRAPH_WIDTH
        context['graphite_server'] = settings.GRAPHITE_SERVER
        context['graphs'] = self.graphdefs()

        period_from = int(mktime(self.object.start.timetuple()))
        period_to   = int(mktime(self.object.stop.timetuple()))

        context['period'] = "from=%s&until=%s" % (period_from, period_to)
        context['target'] = "bench%d" % (self.object.bench.id)
        context['bgcolor'] = "white"
        context['fgcolor'] = "black"
        return context


class LastRunDetailView(RunDetailView):

    def get_context_data(self, **kwargs):
        context = super(RunDetailView, self).get_context_data(**kwargs)
        context['width'] = settings.GRAPH_WIDTH
        context['graphite_server'] = settings.GRAPHITE_SERVER
        context['graphs'] = self.graphdefs()
        context['period'] = "from=-1h"
        context['target'] = "bench%d" % (self.object.bench.id)
        context['bgcolor'] = "white"
        context['fgcolor'] = "black"
        return context


class RunReportDetailView(DetailView):

    model = Run
    template_name = "graph/run_report.html"

    def get_object(self, **kwargs):
        obj = super(RunReportDetailView, self).get_object()
        if self.request.user not in obj.bench.campaign.client.users.all():
            raise PermissionDenied
        else:
            return obj

    def get_context_data(self, **kwargs):
        # Fill the context with additional datas
        #
        context = super(RunReportDetailView, self).get_context_data(**kwargs)
        # Associated bench
        bench = Bench.objects.filter(pk=self.object.bench.pk)

        # Previous run
        try:
            context['prev_run'] = Run.objects.filter(bench=bench,
                                                     start__lt=self.object.start).order_by('-start')[:1][0]
        except IndexError:
            context['prev_run'] = ''
        try:
            context['next_run'] = Run.objects.filter(bench=bench,
                                                     start__gt=self.object.start).order_by('start')[:1][0]
        except IndexError:
            context['next_run'] = ''
        # NFR
        nfr = Nfr.objects.filter(bench=bench)
        requirements = Requirement.objects.filter(nfr=nfr)
        rqx = dict()
        rqwarn = dict()
        for requ in requirements:
            rqx['{}_{}'.format(requ.name,requ.metric)] = requ.crit
            rqwarn['{}_{}'.format(requ.name,requ.metric)] = requ.warn

        measures = []
        oldname = ''
        data = dict()
        points = Measure.objects.filter(run=self.object, classe="transaction").order_by('name')
        for point in points:
            if point.name != oldname:
                measures.append(data)
                data = dict()
                data['name'] = point.name
            data[point.metric] = point.value
            data['{}_class'.format(point.metric)] = 'norm'

            if '{}_{}'.format(point.name,point.metric) in rqwarn:
                if point.value > rqwarn['{}_{}'.format(point.name,point.metric)]:
                    data['{}_class'.format(point.metric)] = 'warn'

            if '{}_{}'.format(point.name,point.metric) in rqx:
                if point.value > rqx['{}_{}'.format(point.name,point.metric)]:
                    data['{}_class'.format(point.metric)] = 'crit'

            oldname = point.name
        measures.append(data)  # add the last one
        measures.remove({})  # remove the first empty

        context['width'] = settings.GRAPH_WIDTH
        context['graphite_server'] = settings.GRAPHITE_SERVER
        context['measures'] = measures
        context['url_stats'] = Measure.objects.filter(run=self.object,metric="url_status_nb")
        context['requirements'] = requirements
        if len(nfr):
            context['nfr'] = nfr[0]
        return context


class BenchReportDetailView(DetailView):

    model = Bench
    template_name = "graph/bench_report.html"

    def get_context_data(self, **kwargs):

        measures = Measure.objects.all()

        context = super(RunReportDetailView, self).get_context_data(**kwargs)
        context['width'] = settings.GRAPH_WIDTH
        context['graphite_server'] = settings.GRAPHITE_SERVER
        context['measures'] = measures

        return context


@login_required
def lastrun(request):

    template_name = "graph/run_detail.html"

    dash_id = 7
    dits = DashItem.objects.filter(dash__pk=dash_id).order_by('order')
    graphs = []
    for dit in dits:
        graphs.append(GraphDefinition.objects.get(pk=dit.graph.id))

    return render(request,
                  template_name,
                  {'width': settings.GRAPH_WIDTH,
                   'graphite_server': settings.GRAPHITE_SERVER,
                   'graphs': graphs,
                   'period': "from=-1h",
                   'target': "bench3",
                   'bgcolor': "white",
                   'fgcolor': "black"})


@login_required
def one_graph(request, pk, host):
    """Only one graph
    """
    template_name = 'one_graph.html'

    qst = request.META['QUERY_STRING']

    if qst.startswith('from='):
        period = qst
    else:
        period = 'from =-1h'

    return render(request,
                  template_name,
                  {'width': 900,
                   'height': 600,
                   'graphite_server': settings.GRAPHITE_SERVER,
                   'graph': GraphDefinition.objects.filter(pk=pk)[0],
                   'bgcolor': "white",
                   'fgcolor': "black",
                   'period': period,
                   'target': Host.objects.filter(pk=host)[0].alphacode,
                   })


def measure_datas(request, bench, metric, name):
    """Measure
    """
    measures = Measure.objects.filter(run__bench__pk=bench,metric=metric,name=name).order_by("run__start")
    alpha = []
    for mes in measures:
        alpha.append(mes.value)

    data = simplejson.dumps(alpha)
    return HttpResponse(data, mimetype='application/json')
