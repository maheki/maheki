#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Consolidate statistics and print some of them on stdout
Useful when requirements are changed after a run or measures uploads
"""
from datetime import datetime
import numpy
from django.core.management.base import BaseCommand
from django.db.models import Count, Max, Min
from maheki.graph.models import Url
from maheki.graph.models import Run
from maheki.graph.models import Bench
from maheki.graph.models import Nfr
from maheki.graph.models import Requirement
from maheki.graph.models import Measure
from maheki.storage.models import Pom

def create_mesure_transaction(run, name, metric, value):

    result = 0

    Measure.objects.create(run=run,
                           name=name,
                           classe='transaction',
                           metric=metric,
                           value=value)

    nfr = Nfr.objects.filter(bench=run.bench)
    if nfr:
        reqs = Requirement.objects.filter(nfr=nfr, name=name, metric=metric)
        if len(reqs):
            if value > reqs[0].warn:
                result = 1
            if value > reqs[0].crit:
                result = 2

    return result


class Command(BaseCommand):
    help = 'Consolidate datas'

    def handle(self, *args, **options):
        """
        Handle the command
        """
        self.update_runs()
        self.update_measures()

    def update_runs(self):
        """
        Update runs datas
        Limit the update to the last 2 runs
        """
        self.stdout.write("===================================\n")
        runs = Run.objects.all().order_by("-pk")[:2]

        for run in runs:
            start_date = Pom.objects.filter(run=run).aggregate(Min('datetms'), Max('datetms'))

            try:
                start = datetime.fromtimestamp(start_date['datetms__min'])
                stop = datetime.fromtimestamp(start_date['datetms__max'])
            except:
                start = datetime.fromtimestamp(0)
                stop = datetime.fromtimestamp(0)

            nburl = Url.objects.filter(run=run).count()
            run.nburls = nburl
            run.start = start
            run.stop = stop
            run.save()
            self.stdout.write("Run #{} : {} urls({}) {}\n".format(run.id,
                                                           run.bench, run.nburls, nburl))

            urls = Url.objects.values('status').annotate(Count('status')).filter(run=run)
            sumurl = 0
            for url in urls:
                sumurl = sumurl + url["status__count"]

            for url in urls:
                Measure.objects.filter(run=run,
                                       name=url["status"],
                                       metric="url_status_nb",).delete()
                Measure.objects.create(
                    run=run,
                    name=url["status"],
                    metric="url_status_nb",
                    value=url["status__count"],
                    percent=float(url["status__count"])*100.0/float(sumurl))

    def update_measures(self):
        """
        Update measures from data stored in pom
        """
        self.stdout.write("===================================\n")
        runs = Run.objects.all().order_by("-pk")[:2]

        for run in runs:
            status = 0
            names = {}
            Measure.objects.filter(run=run).delete()

            poms = Pom.objects.filter(run=run)
            for pom in poms:
                names[pom.name] = pom.name

            for trans in names:
                poms = Pom.objects.filter(run=run, name=trans)
                values = []
                for pom in poms:
                    values.append(pom.value)

                mini = min(values)
                maxi = max(values)

                status = max(status, create_mesure_transaction(run, trans, 'min', mini))
                status = max(status, create_mesure_transaction(run, trans, 'max', maxi))
                status = max(status, create_mesure_transaction(run, trans, 'count', len(values)))
                status = max(status, create_mesure_transaction(run, trans, 'mean', numpy.mean(values)))
                status = max(status, create_mesure_transaction(run, trans, 'stdev', numpy.round(numpy.std(values),2)))
                status = max(status, create_mesure_transaction(run, trans, 'quantile95', numpy.round(numpy.percentile(values, 95),2)))
                status = max(status, create_mesure_transaction(run, trans, 'quantile98', numpy.round(numpy.percentile(values, 98), 2)))

            if status != run.status:
                run.status = status
                run.save()
