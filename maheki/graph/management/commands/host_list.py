#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Print host list on stdout
"""
from django.core.management.base import BaseCommand
from maheki.graph.models import Host


class Command(BaseCommand):
    help = 'Print stats in munin format'

    def handle(self, *args, **options):
        """
        Handle the munin command
        """
        hosts = Host.objects.all().order_by('-pk')

        for host in hosts:

            self.stdout.write("%-3s %-15s %s\n" % (host.id,
                                                   host.name,
                                                   host.alphacode))
