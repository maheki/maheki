#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Update bench datas
"""
import sys
from django.core.management.base import BaseCommand
from maheki.graph.models import Bench
from maheki.graph.models import Run
from datetime import datetime


class Command(BaseCommand):
    help = 'Update bench datas'

    def handle(self, *args, **options):
        """
        Handle the command
        """
        if float(args[0]) >= float(args[1]):
            print "Rrtr"
            sys.exit(1)

        start = datetime.utcfromtimestamp(float(args[0]))
        stop = datetime.utcfromtimestamp(float(args[1]))
        bid = args[2]

        runs = Run.objects.filter(pk=bid)

        if len(runs) == 1:
            run = runs[0]
            run.start = start
            run.stop = stop
            run.save()
        else:
            Run.objects.create(start=start,
                               stop=stop,
                               bench=Bench.objects.get(pk=bid))
            print 'created'
