#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2013,2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Read all the transaction names from a run and fill requirements
Create a nfr if not present
"""
import sys
from django.core.management.base import BaseCommand
from django.db.models import Count
from django.core.cache import cache
from maheki.graph.models import Url
from maheki.graph.models import Requirement, Nfr
from maheki.graph.models import Bench, Run
from maheki.graph.models import Measure


class Command(BaseCommand):
    help = 'Create requirements from run'

    def handle(self, *args, **options):
        """
        Update runs datas
        Limit the update to the last 2 runs
        """
        runs = Run.objects.filter(pk=int(sys.argv[2]))

        for run in runs:
            #look for existent NFR
            nfrs = Nfr.objects.filter(bench=run.bench)

            if len(nfrs) == 0:
                nfr = Nfr.objects.create(name="Auto from run {}".format(run.id),
                                         bench=run.bench)

                measures  = Measure.objects.filter(run=run)

                for mes in measures:
                    req = Requirement.objects.create(nfr=nfr,
                                                     name=mes.name,
                                                     metric=mes.metric,
                                                     warn=self.warn(mes.value),
                                                     crit=self.crit(mes.value))

                    self.stdout.write("New NFR {} {} w:{} c:{} from {}\n".format(req.name,
                                                                                 req.metric,
                                                                                 req.warn,
                                                                                 req.crit,
                                                                                 mes.value))

    def warn(self, value):
        """Compute the warning limit
        """
        return round(value + value / 20)

    def crit(self, value):
        """Compute the critical limit
        """
        return round(value + value / 10)
