# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for views in resa
"""
from django.conf import settings
from django.contrib.sites.models import Site
from django.core import mail
from django.test import TestCase
from django.test import Client as TestClient
from django.http import HttpRequest
from django.contrib.auth.models import User
from django.test import RequestFactory
from datetime import datetime
from maheki.graph.models import Campaign
from maheki.graph.models import Client
from maheki.graph.models import Bench
from maheki.graph.models import Run


class ApiAnonTests(TestCase):  # pylint: disable-msg=R0904
    """
    Test all the views (or try to do)
    """
    def setUp(self):
        """
        Init
        """
        self.client = Client.objects.create(name='sample foo client')

        self.campaign = Campaign.objects.create(name='The default Campaign',
                                                client=self.client,
                                                start=datetime.now(),
                                                description='lorem ipsum')

        self.bench = Bench.objects.create(campaign=self.campaign,
                                          name='The default bench')

        self.run = Run.objects.create(bench=self.bench,
                                      code="2013",
                                      start=datetime.now(),
                                      stop=datetime.now())

    def test_api(self):
        client = TestClient()
        response = client.get('/api/v1/')
        self.assertEqual(response.status_code, 200)

    def test_bench(self):
        """Unauthorized for anonymous
        """
        client = TestClient()
        response = client.get('/api/v1/bench/')
        self.assertEqual(response.status_code, 401)

    def test_client(self):
        """Unauthorized for anonymous
        """
        client = TestClient()
        response = client.get('/api/v1/client/')
        self.assertEqual(response.status_code, 401)

    def test_measure(self):
        """Unauthorized for anonymous
        """
        client = TestClient()
        response = client.get('/api/v1/measure/')
        self.assertEqual(response.status_code, 401)

    def test_nfr(self):
        """Unauthorized for anonymous
        """
        client = TestClient()
        response = client.get('/api/v1/nfr/')
        self.assertEqual(response.status_code, 401)

    def test_pom(self):
        """Unauthorized for anonymous
        """
        client = TestClient()
        response = client.get('/api/v1/pom/')
        self.assertEqual(response.status_code, 401)

    def test_requirement(self):
        """Unauthorized for anonymous
        """
        client = TestClient()
        response = client.get('/api/v1/requirement/')
        self.assertEqual(response.status_code, 401)

    def test_run(self):
        """Unauthorized for anonymous
        """
        client = TestClient()
        response = client.get('/api/v1/run/')
        self.assertEqual(response.status_code, 401)

    def test_software(self):
        """Unauthorized for anonymous
        """
        client = TestClient()
        response = client.get('/api/v1/software/')
        self.assertEqual(response.status_code, 401)

    def test_url(self):
        """Unauthorized for anonymous
        """
        client = TestClient()
        response = client.get('/api/v1/url/')
        self.assertEqual(response.status_code, 401)
