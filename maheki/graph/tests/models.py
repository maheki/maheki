# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Maheki tests on models
"""
from datetime import datetime
from django.test import TestCase
from maheki.graph.models import Bench
from maheki.graph.models import Campaign
from maheki.graph.models import Client
from maheki.graph.models import Dashboard
from maheki.graph.models import GraphDefinition
from maheki.graph.models import Run
from maheki.graph.models import Host
from maheki.graph.models import Source
from maheki.graph.models import Softcat
from maheki.graph.models import HostOption
from maheki.graph.models import Measure


class ModelsTests(TestCase):  # pylint: disable-msg=R0904
    def setUp(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.client = Client.objects.create(name='sample foo client')

        self.host = Host.objects.create(name='sample foo',
                                        client=self.client)

        self.campaign = Campaign.objects.create(name='The default Campaign',
                                                client=self.client,
                                                start=datetime.now(),
                                                description='lorem ipsum')

        self.bench = Bench.objects.create(campaign=self.campaign,
                                          name='The default bench')

    def test_client_string(self):
        self.assertEqual(str(self.client), 'sample foo client')

    def test_campaign_string(self):
        self.assertEqual(str(self.campaign), 'The default Campaign')

    def test_bench_string(self):
        self.assertEqual(str(self.bench), 'The default bench')

    def test_campaign_url(self):
        idc = self.campaign.id
        self.assertEqual(self.campaign.url, '/campaign/{}'.format(idc))

    def test_create_host(self):
        idh = Host.objects.create(name='foo', client=self.client)

        self.assertTrue(idh > 0)

    def test_host_disks(self):
        host = Host.objects.create(name='foo',
                                   client=self.client)
        HostOption.objects.create(host=host,
                                  name='disk_1',
                                  value='vda')
        HostOption.objects.create(host=host,
                                  name='disk_2',
                                  value='vdb')

        newf = Host.objects.filter(pk=host.id)[0]

        self.assertEqual(len(newf.disks()), 2)

    def test_create_campaign(self):
        """Campaign creation
        """
        before = Campaign.objects.all().count()

        camp = Campaign.objects.create(name='foo campaign',
                                       client=self.client,
                                       start=datetime.now(),
                                       description='lorem ipsum')

        after = Campaign.objects.all().count()

        self.assertTrue(camp.id > 0)
        self.assertEqual(after, before + 1)
        self.assertEqual(str(camp), 'foo campaign')

    def test_create_softcat(self):

        before = Softcat.objects.all().count()

        camp = Softcat.objects.create(name='foo soft cat',
                                      description='lorem ipsum')

        after = Softcat.objects.all().count()

        self.assertTrue(camp.id > 0)
        self.assertEqual(after, before + 1)
        self.assertEqual(str(camp), 'foo soft cat')

    def test_create_source(self):

        before = Source.objects.all().count()

        camp = Source.objects.create(name='foo source',
                                     description='lorem ipsum')

        after = Source.objects.all().count()

        self.assertTrue(camp.id > 0)
        self.assertEqual(after, before + 1)
        self.assertEqual(str(camp), 'foo source')

    def test_create_dashboard(self):
        """Dashboard creation
        """
        before = Dashboard.objects.all().count()

        idd = Dashboard.objects.create(name='foo',
                                       description='lorem ipsum')

        after = Dashboard.objects.all().count()

        self.assertTrue(idd > 0)
        self.assertEqual(after, before + 1)

    def test_create_bench(self):
        """
        Create a simple bench
        """
        before = Bench.objects.all().count()

        ben = Bench.objects.create(name='foo bench',
                                   campaign=self.campaign)

        after = Bench.objects.all().count()

        ben.hosts.add(self.host)
        ben.save()

        self.assertTrue(ben.id > 0)
        self.assertEqual(after, before + 1)
        self.assertEqual(str(ben), 'foo bench')

    def test_create_run(self):

        before = Run.objects.all().count()

        idr = Run.objects.create(bench=self.bench,
                                 start=datetime.now(),
                                 stop=datetime.now())

        after = Run.objects.all().count()

        self.assertTrue(idr > 0)
        self.assertEqual(after, before + 1)

    def test_create_graphdefinition(self):

        before = GraphDefinition.objects.all().count()

        gid = GraphDefinition.objects.create(name="foo",
                                             family="main")

        after = GraphDefinition.objects.all().count()

        self.assertTrue(gid > 0)
        self.assertEqual(after, before + 1)

    def test_create_graphurlnoalias(self):
        """Url graph without alias
        """
        gdef = GraphDefinition.objects.create(name="foo",
                                              family="main",
                                              title="footi",
                                              vtitle="foo/sec")

        attend = "target=&vtitle=foo%2Fsec&title=footi"

        self.assertEqual(gdef.url, attend)

    def test_create_graphurlalias(self):

        gdef = GraphDefinition.objects.create(name="foo",
                                              family="main",
                                              title="ti",
                                              vtitle="f/sec",
                                              target="col.foo",
                                              aliases="beta")

        attend = "target=alias(col.foo,%22beta%22)&vtitle=f%2Fsec&title=ti"

        self.assertEqual(gdef.url, attend)

    def test_create_measure(self):
        """Create a simple measure
        """

        idr = Run.objects.create(bench=self.bench,
                                 start=datetime.now(),
                                 stop=datetime.now())

        idm = Measure.objects.create(run=idr,
                                     name="tr_home",
                                     metric="max",
                                     value=4.4)
        self.assertTrue(idm > 0)

    def test_run_status_ok(self):
        """Status grid
        Single measure with status ok
        """
        idr = Run.objects.create(bench=self.bench,
                                 start=datetime.now(),
                                 stop=datetime.now())

        Measure.objects.create(run=idr,
                               name="tr_home",
                               metric="max",
                               value=4.4)

        attend = '<rect x="2" y="2" width="5" height="5" fill="green"/>'

        self.assertEqual(idr.statusgrid, attend)

    def test_run_status_warning(self):
        """Status grid
        Single measure with status warning
        """
        idr = Run.objects.create(bench=self.bench,
                                 start=datetime.now(),
                                 stop=datetime.now())

        Measure.objects.create(run=idr,
                               name="tr_home",
                               metric="max",
                               value=4.4,
                               status=1)

        attend = '<rect x="2" y="2" width="5" height="5" fill="orange"/>'

        self.assertEqual(idr.statusgrid, attend)

    def test_run_status_critical(self):
        """Status grid
        Single measure with status critical
        """

        idr = Run.objects.create(bench=self.bench,
                                 start=datetime.now(),
                                 stop=datetime.now())

        Measure.objects.create(run=idr,
                               name="tr_home",
                               metric="max",
                               value=4.4,
                               status=2)

        attend = '<rect x="2" y="2" width="5" height="5" fill="red"/>'

        self.assertEqual(idr.statusgrid, attend)
