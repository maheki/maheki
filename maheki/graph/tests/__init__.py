from maheki.graph.tests.models import ModelsTests
from maheki.graph.tests.views import ViewsTests
from maheki.graph.tests.listviews import ListViewsTests
from maheki.graph.tests.detailviews import DetailViewsTests
from maheki.graph.tests.api_anon import ApiAnonTests
from maheki.graph.tests.api_auth import ApiAuthTests
from maheki.graph.tests.api_post import ApiPostTests
from maheki.graph.tests.command import CommandTests
