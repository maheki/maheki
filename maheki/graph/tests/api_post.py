# -*- coding: utf-8 -*-
#
# Copyright (c) 2012,2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for api POST function
"""
import urllib
from django.test import TestCase
from django.test.client import Client as TestClient
from django.contrib.auth.models import User
from tastypie.models import ApiKey
from datetime import datetime
from maheki.graph.models import Campaign
from maheki.graph.models import Client
from maheki.graph.models import Bench
from maheki.graph.models import Run
from tastypie.test import ResourceTestCase, TestApiClient


class ApiPostTests(ResourceTestCase):
    """
    Test the API with authentification and POST action
    """
    def setUp(self):
        """
        Init
        """
        Run.objects.all().delete()
        Bench.objects.all().delete()

        self.user = User.objects.create(username='apiuserA')

        self.client = Client.objects.create(name='sample foo client')

        self.client.users.add(self.user.id)

        self.campaign = Campaign.objects.create(name='The default Campaign',
                                                client=self.client,
                                                start=datetime.now(),
                                                description='lorem ipsum')

        self.bench = Bench.objects.create(campaign=self.campaign,
                                          name='The default bench')

    def get_credentials(self):
        return self.create_apikey(username=self.user.username, api_key=self.user.api_key.key)

    def test_run(self):
        """Create a run
        run creation
        """
        tclient = TestApiClient()

        url = '/api/v1/run/'
        data = {"code": "20110401-1840",
                "start": "2011-04-01 18:40",
                "stop": "2011-04-01 19:40",
                "nburls": 0, 
                "status": 0, 
                "number": 0, 
                "valid": False,
                "bench": '/api/v1/bench/{}/'.format(self.bench.id)}

        print url
        print data
        response = tclient.post(url, format='json', data=data)

        self.assertHttpCreated(response)
