# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for Maheki graph management commands
"""
from datetime import datetime
from StringIO import StringIO
from django.core.management import call_command
from django.test import TestCase
from django.contrib.auth.models import User
from maheki.graph.models import Bench
from maheki.graph.models import Client
from maheki.graph.models import Campaign
from maheki.graph.models import Host
from maheki.graph.models import Run


class CommandTests(TestCase):  # pylint: disable-msg=R0904
    """
    Test the management commands in usermgt
    """
    def setUp(self):
        """
        Init
        """
        Run.objects.all().delete()
        Bench.objects.all().delete()
        Host.objects.all().delete()

    def test_munin(self):
        """
        The munin command

        Count number of benchs and run and print result on output
        with munin format
        """
        client = Client.objects.create(name='sample foo client')

        campaign = Campaign.objects.create(name='The default Campaign',
                                                client=client,
                                                start=datetime.now(),
                                                description='lorem ipsum')

        bencha = Bench.objects.create(campaign=campaign, name='bench 1')
        benchb = Bench.objects.create(campaign=campaign, name='bench 2')

        run = Run.objects.create(bench=bencha, code="run1",
                                 start=datetime.now(),
                                 stop=datetime.now())

        run = Run.objects.create(bench=bencha, code="run2",
                                 start=datetime.now(),
                                 stop=datetime.now())

        run = Run.objects.create(bench=benchb, code="run3",
                                 start=datetime.now(),
                                 stop=datetime.now())

        attend = 'bench.value 2\nrun.value 3\n'

        content = StringIO()
        call_command('munin', stdout=content)
        content.seek(0)

        self.assertEqual(content.read(), attend)

    def test_host_list(self):
        """
        host_list management command
        """
        client = Client.objects.create(name='sample foo client')

        Host.objects.create(client=client, name='host1', alphacode='host1')
        Host.objects.create(client=client, name='host2', alphacode='host2')

        attend = '2   host2           host2\n1   host1           host1\n'

        content = StringIO()
        call_command('host_list', stdout=content)
        content.seek(0)

        self.assertEqual(content.read(), attend)
