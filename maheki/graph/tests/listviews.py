# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for views in resa
"""
from django.test import TestCase
from django.contrib.auth.models import User
from django.test import RequestFactory
from datetime import datetime
from maheki.graph.models import Campaign
from maheki.graph.models import Client
from maheki.graph.models import Bench
from maheki.graph.models import Host
from maheki.graph.models import Run
from maheki.graph.models import Nfr
from maheki.graph import views


class ListViewsTests(TestCase):  # pylint: disable-msg=R0904
    """
    Test all the views (or try to do)
    """
    def setUp(self):
        """
        Init
        """
        n_client = Client.objects.create(name='noise client')

        n_campaign = Campaign.objects.create(name='noise Campaign',
                                             client=n_client,
                                             start=datetime.now(),
                                             description='lorem ipsum')

        n_bench = Bench.objects.create(campaign=n_campaign, name='bench')
        Nfr.objects.create(bench=n_bench, name="Noise nfr")

        self.user = User.objects.create(username='user_foo')

        self.client = Client.objects.create(name='sample foo client')

        self.client.users.add(self.user)

        self.campaign = Campaign.objects.create(name='The default Campaign',
                                                client=self.client,
                                                start=datetime.now(),
                                                description='lorem ipsum')

        self.bench = Bench.objects.create(campaign=self.campaign,
                                          name='The default bench')

        self.run = Run.objects.create(bench=self.bench,
                                      code="2013",
                                      start=datetime.now(),
                                      stop=datetime.now())

    def test_campaign(self):
        """Campaign list
        """
        request = RequestFactory().get('/fake-path')
        # We need a user
        request.user = 1
        view = views.CampaignListView.as_view()
        # Run.
        response = view(request)
        # Check.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template_name[0], 'graph/campaign_list.html')
        self.assertEqual(len(response.context_data['object_list']), 1)

    def test_bench(self):
        """Bench
        """
        # Data creation
        user = User.objects.create(username='listviews.py.user_test_bench')
        client = Client.objects.create(name='sample foo client')
        client.users.add(user)

        campaign = Campaign.objects.create(name='Campaign',
                                           client=client,
                                           start=datetime.now(),
                                           description='lorem ipsum')

        Bench.objects.create(campaign=campaign, name='bench')
        Bench.objects.create(campaign=campaign, name='bench')
        Bench.objects.create(campaign=campaign, name='bench')
        Bench.objects.create(campaign=campaign, name='bench')

        request = RequestFactory().get('/fake-path')
        # We need a user
        request.user = user
        view = views.BenchListView.as_view()
        # Run.
        response = view(request)
        # Check.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template_name[0], 'graph/bench_list.html')
        self.assertEqual(len(response.context_data['object_list']), 4)

    def test_nfr(self):
        """NFR
        """
        # Create 2 NFRS for this user
        Nfr.objects.create(bench=self.bench, name="My first nfr")
        Nfr.objects.create(bench=self.bench, name="My second nfr")

        request = RequestFactory().get('/fake-path')
        # We need a user
        request.user = self.user
        view = views.NfrListView.as_view()
        # Run.
        response = view(request)
        # Check.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template_name[0], 'graph/nfr_list.html')
        self.assertEqual(len(response.context_data['object_list']), 2)

    def test_host(self):
        """Host

        - a host is related to one client
        """
        # Data creation
        user = User.objects.create(username='listviews.py.user_test_host')
        client = Client.objects.create(name='sample foo client')
        client.users.add(user)

        Host.objects.create(name='host1', client=client, alphacode='host1')
        Host.objects.create(name='host2', client=client, alphacode='host2')

        client2 = Client.objects.create(name='sample foo client')
        Host.objects.create(name='host3', client=client2, alphacode='host3')

        request = RequestFactory().get('/fake-path')
        # We need a user
        request.user = user
        view = views.HostListView.as_view()
        # Run.
        response = view(request)
        # Check.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template_name[0], 'graph/host_list.html')
        self.assertEqual(len(response.context_data['object_list']), 2)

    def test_graphdef(self):
        """Graph Definition

        - a host is related to one client
        """
        # Data creation
        # datas comes from db initialisation
        request = RequestFactory().get('/fake-path')
        view = views.GraphDefinitionListView.as_view()
        # Run.
        response = view(request)
        # Check.
        tmpl = 'graph/graphdefinition_list.html'
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template_name[0], tmpl)
        self.assertEqual(len(response.context_data['object_list']), 55)
