# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for api v1 with authentification

"""
import json
from django.test import TestCase
from django.test import Client as TestClient
from django.contrib.auth.models import User
from tastypie.models import ApiKey
from datetime import datetime
from maheki.graph.models import Campaign
from maheki.graph.models import Client
from maheki.graph.models import Bench
from maheki.graph.models import Run


class ApiAuthTests(TestCase):
    """
    Test the API witth authentification
    """
    def setUp(self):
        """
        Init
        """
        Run.objects.all().delete()
        Bench.objects.all().delete()

        self.client = Client.objects.create(name='sample foo client')

        self.campaign = Campaign.objects.create(name='The default Campaign',
                                                client=self.client,
                                                start=datetime.now(),
                                                description='lorem ipsum')

        self.bench = Bench.objects.create(campaign=self.campaign,
                                          name='The default bench')

        self.run = Run.objects.create(bench=self.bench,
                                      code="2013",
                                      start=datetime.now(),
                                      stop=datetime.now())

        self.usera = User.objects.create(username='apiuserA')

        self.client.users.add(self.usera.id)

    def test_benchs(self):
        """
        List benchs
        """
        myuser = User.objects.create(username='userA_test_benchs')
        my_client = Client.objects.create(name='My Client')
        my_client.users.add(myuser.id)

        campaign = Campaign.objects.create(name='My Campaign',
                                           client=my_client,
                                           start=datetime.now(),
                                           description='lorem ipsum')

        Bench.objects.create(campaign=campaign, name='My bench')

        tclient = TestClient()
        (apikey, created) = ApiKey.objects.get_or_create(user=myuser)
        apikey.key = apikey.generate_key()
        apikey.save()

        url = '/api/v1/bench/'
        params = {'format': 'json',
                  'username': myuser.username,
                  'api_key': apikey.key}
        response = tclient.get(url, params)
        results = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(results['meta']['total_count'], 1)

    def test_runs(self):
        """
        All runs
        """
        usera = User.objects.create(username='user_test_runs')
        my_client = Client.objects.create(name='My Client')
        my_client.users.add(usera.id)

        campaign = Campaign.objects.create(name='My Campaign',
                                           client=my_client,
                                           start=datetime.now(),
                                           description='lorem ipsum')

        bench = Bench.objects.create(campaign=campaign,
                                     name='My bench')

        run = Run.objects.create(bench=bench,
                                 code="2013redf",
                                 start=datetime.now(),
                                 stop=datetime.now())

        tclient = TestClient()

        apikey = ApiKey.objects.get_or_create(user=usera)[0]
        apikey.key = apikey.generate_key()
        apikey.save()
        url = '/api/v1/run/'
        params = {'format': 'json',
                  'username': usera.username,
                  'api_key': apikey.key}
        response = tclient.get(url, params)
        results = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(results['meta']['total_count'], 1)
        self.assertEqual(int(results['objects'][0]['id']), run.id)
