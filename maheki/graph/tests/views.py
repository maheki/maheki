# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for views in Maheki graph
"""
from json import loads as json_loads
from django.test import TestCase, Client
from django.test import RequestFactory
from datetime import datetime
from maheki.graph.models import Campaign
from maheki.graph.models import Client
from maheki.graph.models import Bench
from maheki.graph.models import Run
from maheki.graph.models import Measure
from maheki.graph import views


class ViewsTests(TestCase):  # pylint: disable-msg=R0904
    """
    Test all the views (or try to do)
    """
    def setUp(self):
        """
        Init
        """
        self.client = Client.objects.create(name='sample foo client')

        self.campaign = Campaign.objects.create(name='The default Campaign',
                                                client=self.client,
                                                start=datetime.now(),
                                                description='lorem ipsum')

        self.bench = Bench.objects.create(campaign=self.campaign,
                                          name='The default bench')

        self.run = Run.objects.create(bench=self.bench,
                                      code="2013",
                                      start=datetime.now(),
                                      stop=datetime.now())

    def test_measure_datas(self):
        """Measure datas

        - return a json
        """
        # Setup name.
        bench = Bench.objects.create(campaign=self.campaign,
                                     name='A simple bench')

        run = Run.objects.create(bench=bench,
                                 code="2104",
                                 start=datetime.now(),
                                 stop=datetime.now())

        Measure.objects.create(run=run,
                               name="tr_home",
                               metric="max",
                               value="4.567")

        bench2 = Bench.objects.create(campaign=self.campaign,
                                      name='A simple bench')

        run2 = Run.objects.create(bench=bench2,
                                  code="run2-2104",
                                  start=datetime.now(),
                                  stop=datetime.now())

        Measure.objects.create(run=run2,
                               name="tr_home",
                               metric="max",
                               value="3.568")

        # Setup request and view.
        request = RequestFactory().get('/fake-path')
        response = views.measure_datas(request, bench.id, "max", "tr_home")
        datas = json_loads(response.content)
        # Check.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')
        self.assertEqual(len(datas), 1)
        self.assertEqual(datas[0], 4.567)
