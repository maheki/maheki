# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for views in resa
"""
from django.core.exceptions import PermissionDenied
from django.test import TestCase
from django.contrib.auth.models import User
from django.test import RequestFactory
from datetime import datetime
from maheki.graph.models import Campaign
from maheki.graph.models import Client
from maheki.graph.models import Bench
from maheki.graph.models import Host
from maheki.graph.models import Run
from maheki.graph.models import Nfr
from maheki.graph.models import Measure
from maheki.graph.models import Requirement
from maheki.graph import views


class DetailViewsTests(TestCase):  # pylint: disable-msg=R0904
    """
    Test all the views (or try to do)
    """
    def setUp(self):
        """
        Init
        """
        n_client = Client.objects.create(name='noise client')

        n_campaign = Campaign.objects.create(name='noise Campaign',
                                             client=n_client,
                                             start=datetime.now(),
                                             description='lorem ipsum')

        n_bench = Bench.objects.create(campaign=n_campaign, name='bench')
        Nfr.objects.create(bench=n_bench, name="Noise nfr")

        self.user = User.objects.create(username='user_foo')

        self.client = Client.objects.create(name='sample foo client')

        self.client.users.add(self.user)

        self.campaign = Campaign.objects.create(name='The default Campaign',
                                                client=self.client,
                                                start=datetime.now(),
                                                description='lorem ipsum')

        self.bench = Bench.objects.create(campaign=self.campaign,
                                          name='The default bench')

        self.run = Run.objects.create(bench=self.bench,
                                      code="2013",
                                      start=datetime.now(),
                                      stop=datetime.now())

    def test_campaign(self):
        """Campaign detail view
        """
        # Data creation
        user = User.objects.create(username='detailviews.user_test_bench')
        client = Client.objects.create(name='sample foo client')
        client.users.add(user)

        campaign = Campaign.objects.create(name='Campaign',
                                           client=client,
                                           start=datetime.now(),
                                           description='lorem ipsum')

        Bench.objects.create(campaign=campaign, name='bench')
        Bench.objects.create(campaign=campaign, name='bench')
        Bench.objects.create(campaign=campaign, name='bench')

        request = RequestFactory().get('/fake-path')
        # We need a user
        request.user = user
        view = views.CampaignDetailView.as_view()
        # Run.
        response = view(request, pk=campaign.id)
        # Check.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template_name[0], 'graph/campaign_detail.html')
        self.assertEqual(len(response.context_data['benchs']), 3)

    def test_campaign_wrong_user(self):
        """Campaign detail view
        """
        # Data creation
        usera = User.objects.create(username='detailviews.usera_test_campaign')
        clienta = Client.objects.create(name='sample foo client')
        clienta.users.add(usera)

        userb = User.objects.create(username='detailviews.userb_test_campaign')
        clientb = Client.objects.create(name='sample foo client')
        clientb.users.add(userb)

        campaignb = Campaign.objects.create(name='Campaign',
                                            client=clientb,
                                            start=datetime.now(),
                                            description='lorem ipsum')

        request = RequestFactory().get('/fake-path')
        # We need a user
        request.user = usera
        view = views.CampaignDetailView.as_view()
        # Check.
        self.assertRaises(PermissionDenied, view, request, pk=campaignb.id)

    def test_host(self):
        """Host detail view
        """
        # Data creation
        usera = User.objects.create(username='detailviews.usera_test_host')
        clienta = Client.objects.create(name='sample foo client')
        clienta.users.add(usera)

        hosta = Host.objects.create(name='Host',
                                    alphacode='hosta',
                                    client=clienta)

        request = RequestFactory().get('/fake-path')
        # We need a user
        request.user = usera
        view = views.HostDetailView.as_view()
        response = view(request, pk=hosta.id)
        # Check.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template_name[0], 'graph/host_detail.html')
        self.assertEqual(len(response.context_data['graphs']), 24)
        self.assertEqual(response.context_data['fgcolor'], 'black')
        self.assertEqual(response.context_data['target'], 'hosta')

    def test_host_wrong_user(self):
        """Host detail view
        """
        # Data creation
        usera = User.objects.create(username='detailviews.usera_test_host_wu')
        clienta = Client.objects.create(name='sample foo client')
        clienta.users.add(usera)

        userb = User.objects.create(username='detailviews.userb_test_host_wu')
        clientb = Client.objects.create(name='sample foo client')
        clientb.users.add(userb)

        hostB = Host.objects.create(name='Host',
                                    client=clientb,
                                    alphacode='hostb')

        request = RequestFactory().get('/fake-path')
        # We need a user
        request.user = usera
        view = views.HostDetailView.as_view()
        # Check.
        self.assertRaises(PermissionDenied, view, request, pk=hostB.id)

    def test_bench_detail_view(self):
        """BenchDetailView.get() sets 'runs' in response context."""
        # Setup name.
        bench = Bench.objects.create(campaign=self.campaign,
                                     name='a simple bench')

        Run.objects.create(bench=bench,
                           code="2104",
                           start=datetime.now(),
                           stop=datetime.now())

        Run.objects.create(bench=bench,
                           code="2105",
                           start=datetime.now(),
                           stop=datetime.now())
        # Setup request and view.
        request = RequestFactory().get('/fake-path')
        view = views.BenchDetailView.as_view()
        # Run.
        response = view(request, pk=bench.id)
        # Check.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template_name[0], 'graph/bench_detail.html')
        self.assertEqual(len(response.context_data['runs']), 2)

    def test_nfr_detail_view(self):
        """Nfr detail view

        - no requirement name specified
        """
        # Setup name.
        bench = Bench.objects.create(campaign=self.campaign,
                                     name='a simple bench')

        nfr = Nfr.objects.create(bench=bench,
                                 name="NFR 2105")

        Requirement.objects.create(nfr=nfr,
                                   name="page",
                                   metric="max",
                                   warn=50,
                                   crit=100)

        Requirement.objects.create(nfr=nfr,
                                   name="page",
                                   metric="mean",
                                   warn=30,
                                   crit=80)

        Requirement.objects.create(nfr=nfr,
                                   name="connect",
                                   metric="max",
                                   warn=50,
                                   crit=100)

        # Setup request and view.
        request = RequestFactory().get('/fake-path')
        view = views.NfrDetailView.as_view()
        # Run.
        response = view(request, pk=nfr.id)
        # Check.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template_name[0], 'graph/nfr_detail.html')
        self.assertEqual(len(response.context_data['requs']), 3)

    def test_nfr_detail_view_with_requ(self):
        """Nfr detail view

        - requirement name specified
        """
        # Setup name.
        bench = Bench.objects.create(campaign=self.campaign,
                                     name='a simple bench')

        nfr = Nfr.objects.create(bench=bench,
                                 name="NFR 2105")

        Requirement.objects.create(nfr=nfr,
                                   name="page",
                                   metric="max",
                                   warn=50,
                                   crit=100)

        Requirement.objects.create(nfr=nfr,
                                   name="page",
                                   metric="mean",
                                   warn=30,
                                   crit=80)

        Requirement.objects.create(nfr=nfr,
                                   name="connect",
                                   metric="max",
                                   warn=50,
                                   crit=100)

        # Setup request and view.
        request = RequestFactory().get('/fake-path')
        view = views.NfrDetailView.as_view()
        # Run.
        response = view(request, pk=nfr.id, name="page")
        # Check.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template_name[0], 'graph/nfr_detail.html')
        self.assertEqual(len(response.context_data['requs']), 2)

    def test_transaction_detail_view(self):
        """Test the transaction view

        - single bench
        - default metric
        - no NFR
        """
        # Setup name.
        bench = Bench.objects.create(campaign=self.campaign,
                                     name='a simple bench')

        run = Run.objects.create(bench=bench,
                                 code="rt2104",
                                 start=datetime.now(),
                                 stop=datetime.now())

        Measure.objects.create(run=run,
                               name="tr_test",
                               metric="max",
                               value="4.567")

        # Setup request and view.
        request = RequestFactory().get('/fake-path')
        view = views.TransactionDetailView.as_view()
        # Run.
        response = view(request, pk=bench.id, trans_name="tr_test")
        # Check.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template_name[0], 'graph/transaction_report.html')
        self.assertEqual(len(response.context_data['runs']), 1)
        self.assertEqual(len(response.context_data['measures']), 1)
