# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Models for application graph in project Maheki
"""
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.http import urlencode
from tastypie.models import create_api_key
from maheki.graph.utils import baseN
from random import random
from maheki.graph.utils import svgsquare


class Client(models.Model):
    """The client as consummer
    """
    name = models.CharField(max_length=300)
    description = models.CharField(max_length=300)
    users = models.ManyToManyField(User)

    def __str__(self):
        return self.name


class Campaign(models.Model):
    """A Campaign is a set of bench
    """
    client = models.ForeignKey(Client)
    name = models.CharField(max_length=300)
    description = models.CharField(max_length=300)
    start = models.DateField()

    def __str__(self):
        return self.name

    @property
    def url(self):
        return '/campaign/{}'.format(self.id)


class Dashboard(models.Model):
    """Dashboard
    """
    name = models.CharField(max_length=300,
                            unique=True)
    description = models.CharField(max_length=300)

    def __str__(self):
        return self.name


class Source(models.Model):
    """Source
    """
    name = models.CharField(max_length=30,
                            unique=True)
    description = models.CharField(max_length=300)

    def __str__(self):
        return self.name


class Softcat(models.Model):
    """Software categories
    """
    name = models.CharField(max_length=30,
                            unique=True)
    description = models.CharField(max_length=300, blank=True)

    def __str__(self):
        return self.name


class Software(models.Model):
    """Software
    """
    name = models.CharField(max_length=30,
                            unique=True)
    description = models.CharField(max_length=300, blank=True)
    category = models.ForeignKey(Softcat)

    def __str__(self):
        return self.name


class Service(models.Model):
    """Service
    """
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=300, blank=True)
    software = models.ForeignKey(Software)

    def __str__(self):
        return self.software.name


class Host(models.Model):
    """A host as computer baremetal or VM
    """
    name = models.CharField(max_length=300)
    alphacode = models.CharField(max_length=8, unique=True)
    options = None
    client = models.ForeignKey(Client)

    services = models.ManyToManyField(Service)

    def get_options(self):
        if self.options is None:
            opts = HostOption.objects.filter(host=self)
            optvals = [(g.name, g.value) for g in opts]
            self.options = optvals

    def disks(self):
        self.get_options()
        values = []
        for opt in self.options:
            if opt[0].startswith('disk_'):
                values.append(opt[1])
        return values

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name


class Bench(models.Model):
    """
    The bench with start and stop time from tsung
    """
    campaign = models.ForeignKey(Campaign)

    name = models.CharField(max_length=300)
    description = models.CharField(max_length=300)
    hosts = models.ManyToManyField(Host, blank=True)
    nbruns = models.IntegerField(default=0, editable=False) # number of runs

    def __unicode__(self):
        return self.name

    @property
    def url(self):
        return '/bench/{}'.format(self.id)


class HostOption(models.Model):
    """Host option
    """
    host = models.ForeignKey(Host)
    name = models.CharField(max_length=300)
    value = models.CharField(max_length=300)

    def __unicode__(self):
        return self.name


class Run(models.Model):
    """
    The bench with start and stop time from tsung
    """
    bench = models.ForeignKey(Bench)
    code = models.CharField(max_length=15, unique=True)
    start = models.DateTimeField()
    stop = models.DateTimeField()
    valid = models.BooleanField(default=True)
    # number of urls
    nburls = models.IntegerField(default=0)
    # in many case 0: ok, 1: warn, 2: critical but can be increased
    status = models.IntegerField(default=0)
    # id of the run, numbers are related to the run
    number = models.IntegerField(default=0, editable=False)

    def __unicode__(self):
        return "%s - %s" % (self.bench.name, self.code)

    @property
    def duration(self):
        return self.stop - self.start

    @property
    def status_class(self):
        statusc = dict()
        statusc[0] = "status-ok"
        statusc[1] = "status-warning"
        statusc[2] = "status-critical"
        return statusc[self.status]

    @property
    def statusgrid(self):
        pos_x = 2
        pos_y = 2
        i = 0
        j = 0
        colors = ["green", "orange", "red"]
        status = ""
        measures = Measure.objects.filter(run__pk=self.id)
        # TODO move this to settings.py
        for mes in measures:
            x = pos_x + i * 5
            y = pos_y + j * 6
            c = colors[mes.status]
            status = status + svgsquare(x, y, c)
            i = i + 1
            if i > 32:
                i = 0
                j = j + 1

        return status


class Url(models.Model):
    """
    Url
    """
    run = models.ForeignKey(Run)
    date = models.FloatField()
    pid = models.CharField(max_length=30)
    userid = models.IntegerField()
    http_method = models.CharField(max_length=10)
    host = models.CharField(max_length=50)
    url = models.CharField(max_length=300)
    status = models.IntegerField()
    size = models.IntegerField()
    duration = models.IntegerField()
    transaction = models.CharField(max_length=300)
    match = models.CharField(max_length=300,blank=True)
    error = models.CharField(max_length=300,blank=True)
    tags = models.CharField(max_length=300,blank=True)

    def __unicode__(self):
        return self.url


class Nfr(models.Model):
    """
    Non functionnal requirements
    """
    bench = models.ForeignKey(Bench)
    name = models.CharField(max_length=300)

    def __unicode__(self):
        return self.name

    @property
    def url(self):
        return '/nfr/{}'.format(self.id)


class Requirement(models.Model):
    """
    Non functionnal requirements
    """
    nfr = models.ForeignKey(Nfr)
    name = models.CharField(max_length=300)
    metric = models.CharField(max_length=30) # mean, max, 95pctl
    warn = models.FloatField()
    crit = models.FloatField()

    class Meta:
        unique_together = ('nfr', 'name', 'metric',)


class Measure(models.Model):
    """

    """
    run = models.ForeignKey(Run)
    name = models.CharField(max_length=300, db_index=True)
    metric = models.CharField(max_length=30)  # mean, max, 95pctl
    value = models.FloatField()
    classe = models.CharField(max_length=30, db_index=True, blank=True)  # transaction
    percent = models.FloatField(blank=True, default=0) # the percent of the item in his sample
    status = models.IntegerField(default=0) # in many case 0: ok, 1: warn, 2: critical but can be increased

    class Meta:
        unique_together = ('run', 'name', 'metric',)


class GraphDefinition(models.Model):
    """
    target=derivative({{object.name}}.tsung.http_200.value)
    """
    name = models.CharField(max_length=50)
    family = models.CharField(max_length=50)
    target = models.CharField(max_length=300)
    title = models.CharField(max_length=50)
    vtitle = models.CharField(max_length=50)
    ymin = models.IntegerField(null=True)
    ymax = models.IntegerField(null=True)
    colorList = models.CharField(max_length=500)
    aliases = models.CharField(max_length=500)
    areaMode = models.CharField(max_length=50)
    yUnitSystem = models.CharField(max_length=50,
                                   choices=(('si','si'),
                                            ('binary','binary')
                                            ),
                                   default='si')

    def __str__(self):
        return self.name

    @property
    def url(self):
        aliases = None
        line = {'title': self.title,
                'vtitle': self.vtitle,}

        if len(self.colorList) > 0:
            line['colorList'] = self.colorList

        if len(self.areaMode) > 0:
            line['areaMode'] = self.areaMode

        if len(self.aliases) > 0:
            aliases = self.aliases.split('|')

        if self.ymin is not None:
            line['yMin'] = self.ymin

        if self.ymax is not None:
            line['yMax'] = self.ymax

        targets = ''
        i = -1
        for trg in self.target.split('|'):
            i += 1
            if aliases is not None:
                targets = targets + "target=alias(%s,%%22%s%%22)&" % (trg, aliases[i])
            else:
                targets = targets + "target=%s&" % trg

        return "%s%s" % (targets,
                         urlencode(line))


class DashItem(models.Model):
    """
    """
    dash = models.ForeignKey(Dashboard)
    graph = models.ForeignKey(GraphDefinition)
    order = models.IntegerField()

@receiver(post_save, sender=Host)
def create_profile(sender, instance, created, **kwargs):
    """Create a matching profile whenever a user object is created."""
    if created:
        if len(instance.alphacode) == 0:
            instance.alphacode = baseN(int(random()*10e8))
            instance.save()

# Performance Killer
# TODO ADD cache
@receiver(post_save, sender=Measure)
def mes_status(sender, instance, created, **kwargs):
    """Update the status regarding to NFR"""
    if created:
        meas = Requirement.objects.filter(nfr__bench__pk=instance.run.bench.id,
                                          metric=instance.metric,
                                          name=instance.name)

        if len(meas) > 0:
            if instance.value > meas[0].warn:
                instance.status = 1
            if instance.value > meas[0].crit:
                instance.status = 2
            instance.save()

#
models.signals.post_save.connect(create_api_key, sender=User)

@receiver(post_save, sender=Run)
def update_bench_stats(sender, instance, created, **kwargs):
    if created:
        idr = 0
        bench = instance.bench
        nb = Run.objects.filter(bench=bench).count()
        bench.nbruns = nb
        bench.save()
        for run in Run.objects.filter(bench=instance.bench).order_by("start"):
            idr = idr + 1
            run.number = idr
            run.save()
