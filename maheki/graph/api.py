# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
API definition
"""
from tastypie import fields
from tastypie.authentication import ApiKeyAuthentication
from tastypie.authorization import DjangoAuthorization
from tastypie.resources import ModelResource, ALL_WITH_RELATIONS
from maheki.graph.models import Bench
from maheki.graph.models import Client
from maheki.graph.models import Run
from maheki.graph.models import Software
from maheki.graph.models import Measure
from maheki.graph.models import Url
from maheki.graph.models import Nfr
from maheki.graph.models import Requirement


class BenchResource(ModelResource):
    """
    The benchs
    """
    class Meta:
        queryset = Bench.objects.all()
        resource_name = 'bench'
        authentication = ApiKeyAuthentication()
        authorization = DjangoAuthorization()

    def apply_authorization_limits(self, request, object_list):
        if request is not None:
            return object_list.filter(campaign__client__users__in=[request.user.id])
        else:
            return object_list.all()

class ClientResource(ModelResource):
    """
    The clients
    """
    class Meta:
        queryset = Client.objects.all()
        resource_name = 'client'
        allowed_methods = ['get']
        authentication = ApiKeyAuthentication()
        authorization = DjangoAuthorization()

    def apply_authorization_limits(self, request, object_list):
        return object_list.filter(users__in=[request.user.id])


class RunResource(ModelResource):
    """
    The runs
    """
    bench = fields.ForeignKey(BenchResource, 'bench')

    class Meta:
        queryset = Run.objects.all()
        resource_name = 'run'
        authentication = ApiKeyAuthentication()
        authorization = DjangoAuthorization()
        filtering = {
            'bench': ALL_WITH_RELATIONS}

    def apply_authorization_limits(self, request, object_list):
        if request is not None:
            return object_list.filter(bench__campaign__client__users__in=[request.user.id])
        else:
            return object_list.all()


class MeasureResource(ModelResource):
    """
    The measures
    """
    run = fields.ForeignKey(RunResource, 'run')

    class Meta:
        queryset = Measure.objects.all()
        resource_name = 'measure'
        excludes = ['id']
        authentication = ApiKeyAuthentication()
        authorization = DjangoAuthorization()
        filtering = {'name': ALL_WITH_RELATIONS,
                     'metric': ALL_WITH_RELATIONS,
                     'value': ALL_WITH_RELATIONS,
                     'run': ALL_WITH_RELATIONS}

    def apply_authorization_limits(self, request, object_list):
        return object_list.filter(run__bench__campaign__client__users__in=[request.user.id])


class UrlResource(ModelResource):
    """
    The urls in a run
    """
    run = fields.ForeignKey(RunResource, 'run')

    class Meta:
        queryset = Url.objects.all()
        resource_name = 'url'
        authentication = ApiKeyAuthentication()
        authorization = DjangoAuthorization()

    def apply_authorization_limits(self, request, object_list):
        return object_list.filter(run__bench__campaign__client__users__in=[request.user.id])


class NfrResource(ModelResource):
    """
    The nfrs in a run
    """
    bench = fields.ForeignKey(BenchResource, 'bench')

    class Meta:
        queryset = Nfr.objects.all()
        resource_name = 'nfr'
        authentication = ApiKeyAuthentication()
        authorization = DjangoAuthorization()

    def apply_authorization_limits(self, request, object_list):
        if request is not None:
            return object_list.filter(bench__campaign__client__users__in=[request.user.id])
        else:
            return object_list.all()


class RequirementResource(ModelResource):
    """
    The requirements in a run
    """
    nfr = fields.ForeignKey(NfrResource, 'nfr')

    class Meta:
        queryset = Requirement.objects.all()
        resource_name = 'requirement'
        authentication = ApiKeyAuthentication()
        authorization = DjangoAuthorization()

    def apply_authorization_limits(self, request, object_list):
        return object_list.filter(nfr__bench__campaign__client__users__in=[request.user.id])


class SoftwareResource(ModelResource):
    """
    The software
    """
    class Meta:
        queryset = Software.objects.all()
        resource_name = 'software'
        authentication = ApiKeyAuthentication()
        authorization = DjangoAuthorization()
        allowed_methods = ['get']
