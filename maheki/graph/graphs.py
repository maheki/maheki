# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
The django views
"""
from mustachebox.backends import BaseBackend

class Backend(BaseBackend):

    def test_method(self, **kwargs):
        """
        render a simple time serie suitable for javascript graphs :
        {
        2004: ['2004', 7160, 546],
        2005: ['2005', 5654, 5435],
        2006: ['2006', 7656, 6545],
        2007: ['2007', 5435, 6545],
        'label': ['year', 'sales', 'expenses']
        }
        """

        #self.template = "area"
        self.template = "barchart"
        #self.template = "multiserie_linechart"

        response = {
            2004: ['2004', 7160, 546],
            2005: ['2005', 5654, 5435],
            2006: ['2006', 7656, 6545],
            2007: ['2007', 5435, 6545],
            'label': ['year', 'sales', 'expenses']
            }

        return response
