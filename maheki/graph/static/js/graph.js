// define dimensions of graph
var m = [10, 10, 20, 40]; // margins
var w = 340 - m[1] - m[3]; // width
var h = 160 - m[0] - m[2]; // height
// create a simple data array that we'll plot with a line (this arrayrepresents only the Y values, X will just be the index location)

function bench_evol(url) {

    d3.select("svg").remove();
    d3.json(url, function(jsondata){

        // X scale will fit all values from data[] within pixels 0-w
        var x = d3.scale.linear().domain([0, jsondata.length - 1]).range([0, w]);

        // automatically determining max range can work something like this
        var y1 = d3.scale.linear().domain([d3.min(jsondata), d3.max(jsondata)]).range([h, 0]);

        var line1 = d3.svg.line()
        // assign the X function to plot our line as we wish
            .x(function(d,i) {
                return x(i);
            })
            .y(function(d) {
                return y1(d);
            })
        // Add an SVG element with the desired dimensions and margin.
        var graph = d3.select("#graph").append("svg:svg")
            .attr("width", w + m[1] + m[3])
            .attr("height", h + m[0] + m[2])
            .append("svg:g")
            .attr("transform", "translate(" + m[3] + "," + m[0] + ")");

        // create xAxis
        var xAxis = d3.svg.axis().scale(x).tickSize(-h).tickSubdivide(false);
        // Add the x-axis.
        graph.append("svg:g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + h + ")")
            .call(xAxis);

        // create left yAxis
        var yAxisLeft = d3.svg.axis().scale(y1).ticks(4).orient("left");
        // Add the y-axis to the left
        graph.append("svg:g")
            .attr("class", "y axis axisLeft")
            .attr("transform", "translate(-5,0)")
            .call(yAxisLeft);

        // do this AFTER the axes above so that the line is above the tick-lines
        graph.append("svg:path").attr("d", line1(jsondata)).attr("class", "data1");
    });
}
/*
 *
 *
 */
function bench_trans(bench, trans_name, metric, warning, critical) {

    var url = "/bench/" + bench + "/metric/" + metric + "/" + trans_name + "/json";

    var m = [10, 0, 20, 50]; // margins
    var w = 640 - m[1] - m[3]; // width
    var h = 480 - m[0] - m[2]; // height

    d3.json(url, function(jsondata){
        
        var maxi = d3.max([warning,critical,d3.max(jsondata)]);

        // X scale will fit all values from data[] within pixels 0-w
        var x = d3.scale.linear().domain([0, jsondata.length - 1]).range([0, w]);
        
        var y1 = d3.scale.linear().domain([0, maxi]).range([h, 0]);

        var line1 = d3.svg.line()
            .x(function(d,i) { return x(i); })
            .y(function(d) { return y1(d); })

        var linew = d3.svg.line()
            .x(function(d,i) { return x(i); })
            .y(function(d) { return y1(warning);  })

        var linec = d3.svg.line()
            .x(function(d,i) { return x(i); })
            .y(function(d) { return y1(critical);  })

        // Add an SVG element with the desired dimensions and margin.
        var graph = d3.select("#transgraph").append("svg:svg")
            .attr("width", w + m[1] + m[3])
            .attr("height", h + m[0] + m[2])
            .append("svg:g")
            .attr("transform", "translate(" + m[3] + "," + m[0] + ")");

        // create xAxis
        var xAxis = d3.svg.axis().scale(x).tickSize(-h).tickSubdivide(false);
        // Add the x-axis.
        graph.append("svg:g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + h + ")")
            .call(xAxis);

        // create left yAxis
        var yAxisLeft = d3.svg.axis().scale(y1).ticks(4).orient("left");

        // Add the y-axis to the left
        graph.append("svg:g")
            .attr("class", "y axis axisLeft")
            .call(yAxisLeft);

        // do this AFTER the axes above so that the line is above the tick-lines
        graph.append("svg:path").attr("d", linew(jsondata)).attr("class", "limitw");
        graph.append("svg:path").attr("d", linec(jsondata)).attr("class", "limitc");
        graph.append("svg:path").attr("d", line1(jsondata)).attr("class", "data1");
    });
}