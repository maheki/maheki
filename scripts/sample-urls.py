#!/usr/bin/python
import urllib
import urllib2
import json
import random

def rdatas(urlid, run):

    stat = random.randrange(0,100)

    status = 200
    if stat > 99:
        status = 503
    if stat > 90:
        status = 500

    data = json.dumps({"date": 1380611467.11307,
                       "pid": "<7273.65.0>",
                       "userid": 1,
                       "http_method": "get",
                       "host": "localhost",
                       "url": "/8/112/%d.png" % (100+urlid),
                       "status": status,
                       "size": 470,
                       "duration": 0.459,
                       "transaction": "tr_first_move",
                       "tags": "images,png,paris",
                       "run": "/api/v1/run/%d/" % (run) })

    return data

def post(run, nb):

    i = 0

    while (i < nb):
        i = i + 1
        data = rdatas(i, run)

        key = '088a295db614f6e8148ad5f9cbd34f364c088412'

        auth = 'ApiKey {}:{}'.format('rodo', key)

        parms = json.dumps({'username': 'rodo',
                            'api_key': key})

        url = 'http://127.0.0.1:8000/api/v1/url/?' + parms

        req = urllib2.Request(url, data)
        f = urllib2.urlopen(req)
        response = f.read()
        print '{} {} {}'.format(i, run, nb)
        f.close()


if __name__ == "__main__":
    post(2,100)
    post(3,50)

