#!/usr/bin/ruby
#
# Wrote in Ruby just for fun :)
#
require 'net/http'
require 'json'
require 'uri'

def rvalue(step)
  maxi = Hash.new
  datas = Array.new

  for x in 0..rand(89+20):
      datas[x] = step+rand(30*step)
  end

  maxi["count"] = datas.length()
  maxi["min"] = datas.min()
  maxi["max"] = datas.max()
  maxi["quantile95"] = 40 + rand(20)
  maxi["quantile98"] = 35 + rand(30)
  maxi["stdev"] = rand(4) + rand(19)/10 + rand(319)/100
  maxi["mean"] = datas.inject{ |sum, el| sum + el }.to_f / datas.size
  maxi["error"] = rand(5) + rand(19)/10

  return maxi
end


uri = URI('http://127.0.0.1:8000/api/v1/measure/')

headers = {
    'Content-Type' => 'application/json'
}

for x in 1..12:

    ["tr_foo", "tr_bar", "tr_home", "tr_search",
     "tr_search_town", "tr_town"].each do |transac|
    
    maxi = rvalue(x)

    ["mean", "stdev", "error", "count", "min", "max", "quantile95", "quantile98"].each do |metric|

    datas = {
      :name => transac,
      :classe => "transaction",
      :metric => metric,
      :value => [0, maxi[metric] - (3 * x)].max(),
      :run => "/api/v1/run/" + x.to_s + "/" }
    
    req = Net::HTTP::Post.new(uri.path, initheader = headers)
    req.body = datas.to_json

    res = Net::HTTP.start(uri.host, uri.port) do |http|
      http.request req
    end
  
    puts res
  end
    end
end

