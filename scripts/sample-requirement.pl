#!/usr/bin/perl -w
use strict;
use LWP::UserAgent;
use HTTP::Request;
use JSON;

my $ua = new LWP::UserAgent;
$ua->agent("Maheki Client (Perl)/1.0");
$ua->default_header("Content-Type" => "application/json");

my $host="localhost";
my $port = "8000";
my $protocol="http";

my $key = $ENV{MAHEKI_KEY};

my $url = "$protocol://$host:$port/api/v1/requirement/?username=rodo\&api_key=$key";


for my $trans (qw(tr_foo tr_bar tr_home tr_search tr_search_town tr_town)) {
    for my $metric (qw(max min mean quantile95 quantile98)) {

        my $datas = {
            "nfr" => "/api/v1/nfr/1/",
            "name" => $trans,
            "metric" => $metric,
            "warn" => 50,
            "crit" => 80};
        
        my $req = HTTP::Request->new( 'POST', $url );
        $req->header( 'Content-Type' => 'application/json' );
        $req->content( JSON::encode_json($datas) );
        
        my $resp = $ua->request( $req );
        print $resp->decoded_content();   
    }
}
