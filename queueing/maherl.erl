%%
%%
-module(maherl).
-include_lib("/home/rodo/soft/gen_bunny/deps/rabbit_common/include/rabbit_framing.hrl").
-export([foo/0,pub/0,stop/0]).

foo()->
    Vhost = <<"maheki">>,
    bunnyc:start_link(mq_producer, 
                      {network, "localhost", 5672, {<<"guest">>, <<"guest">>}, Vhost},
                      {#'exchange.declare'{exchange = <<"celery">>, type = <<"direct">>, durable=true}}, [] ),

    Btask = <<"{'id': '4cc7438e-afd4-4f8f-a2f3-f46567e7ca77', 'task': 'celery.task.maheki-pom.ping',
     'args': [],
     'kwargs': {},
     'retries': 0,
     'eta': '2009-11-17T12:30:56.527191'}">>,
           
    bunnyc:publish(mq_producer, <<"celery">>, Btask, [<<"content-type: application/json">>]),
    bunnyc:stop(mq_producer).


pub()->
    bunnyc:publish(mq_producer, <<"">>, <<"[5,5]">>).

stop()->
    bunnyc:stop(mq_producer).



%% 'maheki-pom.post', [2, 1846, "tr_home", 5.5]

%%(args:('maheki-pom.post', 'c4ac784b-dd8a-41f6-877e-9f9ffd00388c', [2, 1384254258.0, 'tr_home', 5.5], {}) kwargs:{'hostname': 'elz', 'request': {'retries': 0, 'task': 'maheki-pom.post', 'utc': False, 'loglevel': 10, 'delivery_info': {'routing_key': u'celery', 'exchange': u'celery'}, 'args': [2, 1384254258.0, 'tr_home', 5.5], 'expires': None, 'is_eager': False, 'eta': None, 'hostname': 'elz', 'kwargs': {}, 'logfile': None, 'id': 'c4ac784b-dd8a-41f6-877e-9f9ffd00388c'}})
