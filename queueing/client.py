from measures import post
from celery.execute import send_task
import time

value = 5.5

result = send_task('maheki-pom.post', [2, 
                                       time.mktime(time.localtime()), 
                                       "tr_home", 
                                       value])

print result.get()
