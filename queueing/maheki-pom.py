# -*- coding: utf-8 -*-  pylint: disable-msg=R0801
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Send measure by API
"""
from celery.task import task
import urllib2
import json
import time

@task
def post(run, date, transaction, duration):

    send(run, date, transaction, duration)

    return '{} {} {}'.format(run, transaction, date)


@task
def ping(run):

    send(run, time.mktime(time.localtime()), "tr_home", 42.4)

    return '{} {} {}'.format(run, transaction, date)


def send(run, date, transaction, duration):
    data = json.dumps({"datetms": date,
                       "name": transaction,
                       "value": duration,
                       "run": "/api/v1/run/{}/".format(run) })

    url = 'http://127.0.0.1:8000/api/v1/pom/'

    req = urllib2.Request(url, data, {'Content-Type': 'application/json'})
    f = urllib2.urlopen(req)
    response = f.read()
    print response
    f.close()

if __name__ == '__main__':
    send(2, 1380611472.1, "tr_home", 5.5)
